$(document).ready(function () {
	$(window).scroll(function () {
		let scroll = $(this).scrollTop();
		let windowWidth = $(this).outerWidth();
		let contentHeight = $(".content").outerHeight();
		let topBlockHeight = $(".parallax").outerHeight();
		let percentScrollOfContent = (scroll / contentHeight * 100) ^ 0;
		let percentScrollOfTop = (scroll / topBlockHeight * 100) ^ 0;
		let opacity = 1 - 1 / 300 * percentScrollOfTop;

		let zoom_1 = 1 + (windowWidth / 500000 * percentScrollOfContent);
		$('.parallax__fog').css('transform', `scale(${zoom_1})`);
		// console.log(opacity);
		$('.parallax__fog').css("opacity", opacity);

		let zoom_2 = 1 + (windowWidth / 500000 * percentScrollOfContent);
		// console.log(zoom_2);
		$(".parallax__mountain_1").css('transform', `scale(${zoom_2})`);
		// let horiz = windowWidth / 2000 * percentScrollOfContent

		let horiz = windowWidth / 2000 * percentScrollOfTop;
		let zoom_3 = 1 + (windowWidth * 0.00004 * percentScrollOfContent);
		$(".parallax__mountain_2").css('transform', `translate3d(${horiz}px,0,0) scale(${zoom_3})`);

		let horiz_2 = windowWidth / 1600 * percentScrollOfTop;
		let zoom_4 = 1 + (windowWidth * 0.00005 * percentScrollOfContent);
		$(".parallax__mountain_3").css('transform', `translate3d(${-horiz_2}px,0,0) scale(${zoom_4})`);

	});
});